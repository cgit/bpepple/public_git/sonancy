/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009 Brian Pepple
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 * 
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>

#include <glib.h>
#include <gtk/gtk.h>
#include <unique/unique.h>

static void
on_window_destroy (GtkWidget  *widget,
		   gpointer    user_data)
{
	gtk_main_quit ();
}

static UniqueResponse
unique_app_message_cb (UniqueApp         *unique_app,
		       gint               command,
		       UniqueMessageData *data,
		       guint              timestamp,
		       gpointer           user_data)
{
	GtkWindow      *window = user_data;
	UniqueResponse  res;

	switch (command) {
	case UNIQUE_ACTIVATE:
		gtk_window_set_screen (GTK_WINDOW (window),
				       unique_message_data_get_screen (data));
		gtk_window_set_startup_id (GTK_WINDOW (window),
					   unique_message_data_get_startup_id (data));
		gtk_window_present_with_time (GTK_WINDOW (window),
					      timestamp);
		res = UNIQUE_RESPONSE_OK;
		break;
	default:
		res = UNIQUE_RESPONSE_OK;
		break;
	}
	return res;
}

int
main (int   argc,
      char *argv[])
{
	UniqueApp  *unique_app;
	GtkWidget  *window;

	gtk_init (&argc, &argv);

	unique_app = unique_app_new ("org.gnome.Sonancy", NULL);

	if (unique_app_is_running (unique_app)) {
		UniqueResponse response;

		response = unique_app_send_message (unique_app,
						    UNIQUE_ACTIVATE,
						    NULL);
		g_object_unref (unique_app);

		return EXIT_SUCCESS;
	}

	window = (GtkWidget *) sonancy_window_new ();
	g_signal_connect (window,
			  "destroy", G_CALLBACK (on_window_destroy),
			  NULL);


	unique_app_watch_window (unique_app, GTK_WINDOW (window));

	g_signal_connect (unique_app, "message-received",
			  G_CALLBACK (unique_app_message_cb),
			  window);

	gtk_main ();

	g_object_unref (unique_app);

	return EXIT_SUCCESS;
}
