/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009 Brian Pepple
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 * 
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include <glib/gi18n.h>

#include <glib-object.h>

#include <gtk/gtk.h>

#include "sonancy-window.h"

#define SONANCY_WINDOW_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), SONANCY_TYPE_WINDOW, SonancyWindowPrivate))

struct _SonancyWindowPrivate {
	GtkWidget *vbox;
	GtkWidget *menubar;
	GtkWidget *file_chooser;
	GtkWidget *subdir_checkbutton;

	GtkUIManager *manager;
	GtkActionGroup *action_group;
};

G_DEFINE_TYPE (SonancyWindow, sonancy_window, GTK_TYPE_WINDOW);

static void
sonancy_window_dispose (GObject *gobject)
{
	SonancyWindowPrivate *priv = SONANCY_WINDOW (gobject)->priv;

	if (priv->manager) {
		g_object_unref (priv->manager);
		priv->manager = NULL;
	}

	if (priv->action_group) {
		g_object_unref (priv->action_group);
		priv->action_group = NULL;
	}

	G_OBJECT_CLASS (sonancy_window_parent_class)->dispose (gobject);
}

static void
sonancy_window_constructed (GObject *gobject)
{
	SonancyWindow        *window = SONANCY_WINDOW (gobject);
	SonancyWindowPrivate *priv = window->priv;

	gtk_widget_show_all (GTK_WIDGET (window));
}

static void
sonancy_window_cmd_quit (GtkAction     *action,
			 SonancyWindow *window)
{
	gtk_widget_destroy (GTK_WIDGET (window));
}

static void
sonancy_window_cmd_preferences (GtkAction     *action,
				SonancyWindow *window)
{

}

static void
about_url_hook (GtkAboutDialog *dialog,
		const gchar    *link_,
		gpointer        user_data)
{
	GdkScreen  *screen;
	gint        pid;
	GError     *error;
	gchar     **argv;

	if (gtk_widget_has_screen (GTK_WIDGET (dialog)))
		screen = gtk_widget_get_screen (GTK_WIDGET (dialog));
	else
		screen = gdk_screen_get_default ();

	argv = g_new (gchar*, 3);
	argv[0] = g_strdup ("gnome-open");
	argv[1] = g_strdup (link_);
	argv[2] = NULL;

	error = NULL;
	gdk_spawn_on_screen (screen,
			     NULL,
			     argv, NULL,
			     G_SPAWN_SEARCH_PATH,
			     NULL, NULL,
			     &pid, &error);

	if (error) {
		g_warning ("Unable to launch gnome-open: %s", error->message);
		g_error_free (error);
	}

	g_strfreev (argv);
}

static void
sonancy_window_cmd_help_about (GtkAction     *action,
			       SonancyWindow *window)
{
	const gchar *authors[] = {
		"Brian Pepple <bpepple@fedoraproject.org>",
		NULL
	};

	const gchar *translator_credits = _("translator-credits");
	const gchar *copyright = "Copyright \xc2\xa9 2009-2010 Brian Pepple";

	gtk_about_dialog_set_url_hook (about_url_hook, NULL, NULL);

	gtk_show_about_dialog (GTK_WINDOW (window),
			       "program-name", PACKAGE_NAME,
			       "title",_("About Sonancy"),
			       "comments", _("Audio Tagging Tool"),
			       "version", VERSION,
			       "copyright", copyright,
			       "authors", authors,
			       "translator-credits", translator_credits,
			       NULL);
}

static void
sonancy_window_class_init (SonancyWindowClass *klass)
{
	GObjectClass   *gobject_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	g_type_class_add_private (klass, sizeof (SonancyWindowPrivate));

	gobject_class->dispose = sonancy_window_dispose;
	gobject_class->constructed = sonancy_window_constructed;
}

static const GtkActionEntry action_entries[] = {
	{ "SonancyFileAction", NULL, N_("_File") },
	{
		"SonancyQuit", GTK_STOCK_QUIT, NULL, "<control>Q",
		N_("Quit Sonancy"),
		G_CALLBACK (sonancy_window_cmd_quit)
	},

	{ "SonancyEditAction", NULL, N_("_Edit") },
	{
		"SonancyPreferences", GTK_STOCK_PREFERENCES, NULL, NULL,
		N_("Edit Sonancy Preferences"),
		G_CALLBACK (sonancy_window_cmd_preferences)
	},

	{ "SonancyHelpAction", NULL, N_("_Help") },
	{
		"SonancyAbout", GTK_STOCK_ABOUT, N_("_About"), NULL, NULL,
		G_CALLBACK (sonancy_window_cmd_help_about)
	}
};

static void
sonancy_window_init (SonancyWindow *window)
{
	SonancyWindowPrivate *priv;
	GtkWidget            *hbox, *frame, *dir_frame, *file_frame;
	GtkWidget            *dir_vbox, *file_vbox;
	GtkAccelGroup        *accel_group;
	GError               *error;

	GTK_WINDOW (window)->type = GTK_WINDOW_TOPLEVEL;
	gtk_window_set_resizable (GTK_WINDOW (window), TRUE);
	gtk_window_set_default_size (GTK_WINDOW (window), 362, 600);
	gtk_window_set_title (GTK_WINDOW (window), "Sonancy");

	window->priv = priv = SONANCY_WINDOW_GET_PRIVATE (window);

	priv->vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (window), priv->vbox);
	gtk_widget_show (priv->vbox);

	priv->action_group = gtk_action_group_new ("SonancyActions");
	gtk_action_group_set_translation_domain (priv->action_group, NULL);
	gtk_action_group_add_actions (priv->action_group,
				      action_entries,
				      G_N_ELEMENTS (action_entries),
				      window);

	priv->manager = gtk_ui_manager_new ();
	gtk_ui_manager_insert_action_group (priv->manager,
					    priv->action_group,
					    0);

	accel_group = gtk_ui_manager_get_accel_group (priv->manager);
	gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

	error = NULL;
	if (!gtk_ui_manager_add_ui_from_file (priv->manager,
					      PKGDATADIR G_DIR_SEPARATOR_S "sonancy.ui",
					      &error))
	{
		g_critical ("Building menus failed: %s",
			    error->message);
		g_error_free (error);
	} else {
		priv->menubar = gtk_ui_manager_get_widget (priv->manager, "/SonancyMenubar");
		gtk_box_pack_start (GTK_BOX (priv->vbox), priv->menubar, FALSE, FALSE, 0);
		gtk_widget_show (priv->menubar);
	}

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (priv->vbox), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	file_vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), file_vbox, FALSE, FALSE, 0);
	gtk_widget_show (file_vbox);

	dir_frame = gtk_frame_new (_("Directory"));
	gtk_frame_set_shadow_type (GTK_FRAME (dir_frame), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (file_vbox), dir_frame);
	gtk_widget_show (dir_frame);

	dir_vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (dir_frame), dir_vbox);

	priv->file_chooser =
		gtk_file_chooser_button_new (_("Audio File Directory"),
					     GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);
	gtk_box_pack_start (GTK_BOX (dir_vbox), priv->file_chooser, FALSE, FALSE, 0);
	gtk_widget_show (priv->file_chooser);

	priv->subdir_checkbutton =
		gtk_check_button_new_with_label (_("Include subdirectories"));
	gtk_box_pack_start (GTK_BOX (dir_vbox), priv->subdir_checkbutton,
			    FALSE, FALSE, 0);
	gtk_widget_show (priv->subdir_checkbutton);

	file_frame = gtk_frame_new (_("Files"));
	gtk_frame_set_shadow_type (GTK_FRAME (file_frame), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (file_vbox), file_frame);
	gtk_widget_show (file_frame);

	/* TODO: Add file listview widget */

	frame = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (hbox), frame);
	gtk_widget_show (frame);
}

GtkWidget *
sonancy_window_new (void)
{
	return g_object_new (SONANCY_TYPE_WINDOW, NULL);
}
