/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2009 Brian Pepple
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 * 
 */

#ifndef __SONANCY_WINDOW_H__
#define __SONANCY_WINDOW_H__

#include <gtk/gtkwindow.h>

G_BEGIN_DECLS

#define SONANCY_TYPE_WINDOW               (sonancy_window_get_type ())
#define SONANCY_WINDOW(obj)               (G_TYPE_CHECK_INSTANCE_CAST ((obj), SONANCY_TYPE_WINDOW, SonancyWindow))
#define SONANCY_IS_WINDOW(obj)            (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SONANCY_TYPE_WINDOW))
#define SONANCY_WINDOW_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), SONANCY_TYPE_WINDOW, SonancyWindowClass))
#define SONANCY_IS_WINDOW_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), SONANCY_TYPE_WINDOW))
#define SONANCY_WINDOW_GET_CLASS(obj)     (G_TYPE_INSTANCE_GET_CLASS ((obj), SONANCY_TYPE_WINDOW, SonancyWindowClass))

typedef struct _SonancyWindow             SonancyWindow;
typedef struct _SonancyWindowPrivate      SonancyWindowPrivate;
typedef struct _SonancyWindowClass        SonancyWindowClass;

struct _SonancyWindow
{
	GtkWindow             parent_instance;

	SonancyWindowPrivate *priv;
};

struct _SonancyWindowClass
{
	GtkWindowClass parent_class;
};

GType      sonancy_window_get_type (void) G_GNUC_CONST;
GtkWidget *sonancy_window_new      (void);

G_END_DECLS

#endif /* __SONANCY_WINDOW_H__ */
